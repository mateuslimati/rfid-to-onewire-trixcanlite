/*
 * oneWireSlave.h
 *
 *  Created on: 3 de ago de 2018
 *      Author: mateus-trix
 */

#ifndef ONEWIRESLAVE_H_
#define ONEWIRESLAVE_H_


/** DEFINES *********************************************************/

#define READ_COMMAND_DS2411  	0x33         // Command to read the 64-bit serial number from 1-wire slave device.
#define	HIGH					0x01
#define	LOW						0x00
#define SET						0x01
#define CLEARBIN				0x00
#define FIRST_BIT				0x03


#define oneWirePin 				GPIO_Pin_1
#define oneWirePort 			GPIOB

/** PROTOTYPES ******************************************************/

unsigned char crc8(const uint8_t *addr, uint8_t len);
void OWS_drive_low();
void OWS_drive_high();
unsigned char OWS_drive_read();
void OWS_write_bit(unsigned char write_bit);
unsigned char OWS_read_bit();
void OWS_write_byte(unsigned char write_data, unsigned char first);
unsigned char OWS_read_byte();
void OWS_present();
unsigned char OWS_wait_reset();
unsigned char * RFID_to_iButton(unsigned char *id);
void send_iButton_code(unsigned char *id);


#endif /* ONEWIRESLAVE_H_ */
